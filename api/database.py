from sqlalchemy import MetaData, create_engine
import databases

DATABASE_URL = "sqlite:///./article.db"

engine = create_engine(
    DATABASE_URL, connect_args={"check_same_thread": False})

database = databases.Database(DATABASE_URL)
metadata = MetaData()
