from pydantic import BaseModel
from typing import Optional


class Article(BaseModel):
    title: str
    description: str


class User(BaseModel):
    email: str
    password: str


class Login(BaseModel):
    username: str
    password: str


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    email: Optional[str] = None
