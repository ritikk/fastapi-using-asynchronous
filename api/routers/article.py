from fastapi import APIRouter, status, HTTPException, Depends
from .. import schemas, models, oauth2
from ..database import database

router = APIRouter(
    tags=["Articles"]
)


@router.post('/article', status_code=status.HTTP_201_CREATED)
async def create_article(request: schemas.Article, get_current_user: schemas.User = Depends(oauth2.get_current_user)):
    query = models.articles.insert().values(title=request.title, description=request.description)
    last_record_id = await database.execute(query)
    return {**request.dict(), "id": last_record_id}


@router.get('/articles')
async def get_all_articles(get_current_user: schemas.User = Depends(oauth2.get_current_user)):
    query = models.articles.select()

    output = await database.fetch_all(query)

    if output == []:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="There is no article in the database right now. Kindly create a article")

    return output


@router.get('/article/{id}')
async def get_article(id: int, get_current_user: schemas.User = Depends(oauth2.get_current_user)):
    query = models.articles.select().where(id == models.articles.c.id)
    article = await database.fetch_one(query)
    if not article:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"The article with the id {id} is not in the database")

    return article


@router.put('/article/{id}')
async def update_article(id: int, request: schemas.Article,
                         get_current_user: schemas.User = Depends(oauth2.get_current_user)):
    query = models.articles.select().where(id == models.articles.c.id)
    article = await database.fetch_one(query)
    if not article:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"The article with the id {id} is not in the database")

    query = models.articles.update().where(id == models.articles.c.id).values(title=request.title,
                                                                              description=request.description)
    await database.execute(query)
    return f"ID {id} is successfully updated"


@router.delete('/article/{id}')
async def delete_article(id: int, get_current_user: schemas.User = Depends(oauth2.get_current_user)):
    query = models.articles.select().where(id == models.articles.c.id)
    article = await database.fetch_one(query)
    if not article:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"The article with the id {id} is not in the database")

    query = models.articles.delete().where(id == models.articles.c.id)
    await database.execute(query)
    return f"ID {id} is successfully deleted"
