from fastapi import APIRouter, status, HTTPException, Depends
from .. import schemas, models, oauth2
from ..database import database
from passlib.context import CryptContext

router = APIRouter(
    tags=["Users"]
)

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def verify(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)


@router.post('/user', status_code=status.HTTP_201_CREATED)
async def create_user(request: schemas.User, get_current_user: schemas.User = Depends(oauth2.get_current_user)):
    existing_user = models.users.select().where(request.email == models.users.c.email)
    user = await database.fetch_one(existing_user)
    if user:
        return f"The user with the username {request.email} already exists"
    hashed_password = pwd_context.hash(request.password)
    query = models.users.insert().values(email=request.email, password=hashed_password)
    last_record_id = await database.execute(query)
    return {**request.dict(), "id": last_record_id}


@router.get('/users')
async def get_all_user(get_current_user: schemas.User = Depends(oauth2.get_current_user)):
    query = models.users.select()
    output = await database.fetch_all(query)

    if output == []:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="There is no user in the database. Please create a user first")

    return await database.fetch_all(query)


@router.delete("/user/{id}")
async def delete_user(id: int, get_current_user: schemas.User = Depends(oauth2.get_current_user)):
    query = models.users.select().where(id == models.users.c.id)
    user = await database.fetch_one(query)
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail=f"The user with the id {id} is not in the database")

    query = models.users.delete().where(id == models.users.c.id)
    await database.execute(query)
    return f"ID {id} is successfully deleted"
