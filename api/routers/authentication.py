from fastapi import APIRouter, HTTPException, status, Depends
from .. import models, token
from ..database import database
from .user import verify
from fastapi.security import OAuth2PasswordRequestForm

router = APIRouter(
    tags=["Authentication"]
)


@router.post("/login")
async def login(request: OAuth2PasswordRequestForm = Depends()):
    query = models.users.select().where(models.users.c.email == request.username)
    user = await database.fetch_one(query)
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Invalid Username")

    if not verify(request.password, user.password):
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Invalid Password")

    access_token = token.create_access_token(
        data={"sub": user.email}
    )
    return {"access_token": access_token, "token_type": "bearer"}
