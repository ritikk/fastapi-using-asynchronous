from sqlalchemy import Table, Column, Integer, String
from .database import metadata

articles = Table(
    "articles",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("title", String(100)),
    Column("description", String(500))
)

users = Table(
    "users",
    metadata,
    Column("id", Integer, primary_key=True),
    Column("email", String(500)),
    Column("password", String(500))
)
