from fastapi import FastAPI
from .database import metadata, database, engine
from .routers import article, user, authentication

metadata.create_all(engine)
app = FastAPI()
app.include_router(article.router)
app.include_router(user.router)
app.include_router(authentication.router)


@app.on_event("startup")
async def startup():
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()
